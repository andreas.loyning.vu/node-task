const express = require('express');
const { read } = require('fs');
const readline = require('readline');
const path = require('path');
const fs = require('fs');
const os = require('os');

const app = express();
const { PORT = 3000 } = process.env;


app.get('/', (req, res) => {
    return res.send('Hello, world!');
})

const promt = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

promt.question(`
    Choose an option:
    1. Read package.json
    2. Display OS infro
    3. Start HTTP server
    Type a number: `, answer => {
    switch(answer) {
        case '1':
            displayPackageJson();
            promt.close();
            break;
        case '2':
            displayOSInfo();
            promt.close();
            break;
        case '3':
            startHTTPServer();
            promt.close();
            break;
        default:
            promt.write('Invalid option.');
            promt.close();
            break;
    }
})

function displayPackageJson() {
    fs.readFile(
        path.join(__dirname, 'package.json'), 'utf-8',
        (err, content) => {
            console.log(content)
            return content.toString();
        }
    )
}

function displayOSInfo() {
    const info = `
    Getting OS info...
    SYSTEM MEMORY = ${(os.totalmem / 1024 / 1025 / 1024).toFixed(2) + ' GB'}
    CPU CORES: ${os.cpus().length}
    ARCH: ${os.arch()}
    PLATFORM: ${os.platform()}
    RELEASE: ${os.release()}
    USER: ${os.userInfo().username}
    `
    console.log(info);
}

function startHTTPServer() {
    app.listen(PORT, () => {
        console.log(`Starting started on port ${PORT}`);
    });
}
